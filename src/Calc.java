import java.util.Scanner;

public class Calc {
    public static void main(String[] args){
        int number1, number2;

        String Operator;

        Scanner input = new Scanner(System.in);

        System.out.println("First number: ");
        number1 = input.nextInt();

        System.out.println("Second number");
        number2 = input.nextInt();

        System.out.println("Select Operator [* , / , + , -]: ");
        Operator = input.next();

        if (Operator.equals("+")){
            System.out.println("Answer : "+(number1+number2));
        }
        if (Operator.equals("-")){
            System.out.println("Answer : " +(number1-number2));
        }
        if (Operator.equals("*")){
            System.out.println("Answer : " +(number1*number2));
        }
        if (Operator.equals("/")) {
            System.out.println("Answer : " +(number1/number2));
        }

    }
}
